package com.example.cocktaildb_fragments.model

import com.example.cocktaildb_fragments.model.local.Category
import com.example.cocktaildb_fragments.model.local.CategoryDrinks
import com.example.cocktaildb_fragments.model.local.Drink
import com.example.cocktaildb_fragments.model.mapper.CategoryDrinksMapper
import com.example.cocktaildb_fragments.model.mapper.CategoryMapper
import com.example.cocktaildb_fragments.model.mapper.DrinkMapper
import com.example.cocktaildb_fragments.model.remote.CocktailService
import com.example.cocktaildb_fragments.model.remote.dtos.categories.CategoryResponse
import com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks.CategoryDrinksResponse
import com.example.cocktaildb_fragments.model.remote.dtos.drinks.DrinksResponse
import com.example.cocktaildb_fragments.utils.Resource
import javax.inject.Inject
import retrofit2.Response

    class CocktailRepo @Inject constructor(
        private val service: CocktailService
    ){
        private val mapper: CategoryMapper = CategoryMapper()
        private val categoryMapper: CategoryDrinksMapper = CategoryDrinksMapper()
        private val drinksMapper: DrinkMapper = DrinkMapper()

        suspend fun getCategories(): Resource<List<Category>> {
            val fetchResponse: Response<CategoryResponse> =
                service.getAllCategories()

            return  if (fetchResponse.isSuccessful && fetchResponse.body() != null){
                val categoryResponse = fetchResponse.body()!!
                val categoryList: List<Category> = categoryResponse.drinks
                    .map { mapper(it) }
                Resource.Success(categoryList)
            } else {
                Resource.Error(fetchResponse.message())
            }
        }

        suspend fun getCategoryDrinks(c: String?): Resource<List<CategoryDrinks>> {
            val fetchResponse: Response<CategoryDrinksResponse> =
                service.getCategoryDrinks(c = c)

            return if (fetchResponse.isSuccessful && fetchResponse.body() != null){
                val drinksInCategoryResponse =
                    fetchResponse.body()?.drinks ?: emptyList()
                Resource.Success(
                    drinksInCategoryResponse.map { categoryMapper(it) }
                )
            } else {
                Resource.Error(fetchResponse.message())
            }
        }

        suspend fun getDrinks(s: String?): Resource<List<Drink>>{
            val fetchResponse: Response<DrinksResponse> =
                service.getDrink(s=s)

            return if (fetchResponse.isSuccessful && fetchResponse.body() != null){
                val drinksResponse =
                    fetchResponse.body()?.drinks ?: emptyList()
                Resource.Success(
                    drinksResponse.map { drinksMapper(it) }
                )
            } else {
                Resource.Error(fetchResponse.message())
            }
    }
}