package com.example.cocktaildb_fragments.model.local

data class CategoryDrinks (
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)