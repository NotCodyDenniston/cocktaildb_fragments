package com.example.cocktaildb_fragments.model.remote

sealed class NetworkResponse<T> {
    sealed class Success<T>(
        val successList: List<T>
    ) : NetworkResponse<List<T>>() {

        data class CategorySuccess<Category>(val categoryList: List<Category>) :
            Success<Category>(categoryList)

        data class CategoryMealSuccess<CategoryMeal>(val mealsInCategory: List<CategoryMeal>) :
            Success<CategoryMeal>(mealsInCategory)

    }
    data class Error(val message: String) : NetworkResponse<String>()
}