package com.example.cocktaildb_fragments.model.remote.dtos.categories

import com.example.cocktaildb_fragments.model.remote.dtos.categories.CategoryDTO

data class CategoryResponse(
    val drinks: List<CategoryDTO>
)