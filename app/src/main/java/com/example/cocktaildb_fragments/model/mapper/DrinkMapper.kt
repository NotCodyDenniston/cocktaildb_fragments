package com.example.cocktaildb_fragments.model.mapper

import com.example.cocktaildb_fragments.model.local.Drink
import com.example.cocktaildb_fragments.model.remote.dtos.drinks.DrinkDTO

class DrinkMapper: Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink = with(dto){
        Drink(
         dateModified,
         idDrink,
         strAlcoholic,
         strCategory,
         strCreativeCommonsConfirmed,
         strDrink,
         strDrinkAlternate,
         strDrinkThumb,
         strGlass,
         strIBA,
         strImageAttribution,
         strImageSource,
         strIngredient1,
         strIngredient10,
         strIngredient11,
         strIngredient12,
         strIngredient13,
         strIngredient14,
         strIngredient15,
         strIngredient2,
         strIngredient3,
         strIngredient4,
         strIngredient5,
         strIngredient6,
         strIngredient7,
         strIngredient8,
         strIngredient9,
         strInstructions,
         strMeasure1,
         strMeasure10,
         strMeasure11,
         strMeasure12,
         strMeasure13,
         strMeasure14,
         strMeasure15,
         strMeasure2,
         strMeasure3,
         strMeasure4,
         strMeasure5,
         strMeasure6,
         strMeasure7,
         strMeasure8,
         strMeasure9,
         strTags
        )
    }
}