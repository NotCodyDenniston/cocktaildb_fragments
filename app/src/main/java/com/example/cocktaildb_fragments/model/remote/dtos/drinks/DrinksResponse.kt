package com.example.cocktaildb_fragments.model.remote.dtos.drinks

data class DrinksResponse(
    val drinks: List<DrinkDTO>
)