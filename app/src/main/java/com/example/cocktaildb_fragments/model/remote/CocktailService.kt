package com.example.cocktaildb_fragments.model.remote

import com.example.cocktaildb_fragments.model.remote.dtos.categories.CategoryResponse
import com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks.CategoryDrinksResponse
import com.example.cocktaildb_fragments.model.remote.dtos.drinks.DrinksResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getAllCategories(@Query("c")c:String="list"): Response<CategoryResponse>

    @GET(CATEGORYDRINKS_ENDPOINT)
    suspend fun getCategoryDrinks(@Query("c")c:String?="Ordinary_Drink"): Response<CategoryDrinksResponse>

    @GET(DRINK_ENDPOINT)
    suspend fun getDrink(@Query("s")s:String?="margarita"): Response<DrinksResponse>

    companion object {


        private const val CATEGORY_ENDPOINT = "list.php"
        private const val CATEGORYDRINKS_ENDPOINT = "filter.php"
        private const val DRINK_ENDPOINT = "search.php"
    }
}