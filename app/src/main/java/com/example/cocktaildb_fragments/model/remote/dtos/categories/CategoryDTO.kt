package com.example.cocktaildb_fragments.model.remote.dtos.categories

data class CategoryDTO(
    val strCategory: String
)