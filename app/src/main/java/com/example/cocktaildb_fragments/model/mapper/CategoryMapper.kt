package com.example.cocktaildb_fragments.model.mapper

import com.example.cocktaildb_fragments.model.local.Category
import com.example.cocktaildb_fragments.model.remote.dtos.categories.CategoryDTO

class CategoryMapper: Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto){
        Category(
            strCategory
        )
    }
}