package com.example.cocktaildb_fragments.model.mapper

interface Mapper<in DTO, out ENTITY> {
    operator fun invoke(dto: DTO):ENTITY
}