package com.example.cocktaildb_fragments.model.mapper

import com.example.cocktaildb_fragments.model.local.CategoryDrinks
import com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks.CategoryDrinkDTO

class CategoryDrinksMapper: Mapper<CategoryDrinkDTO, CategoryDrinks> {
    override fun invoke(dto: CategoryDrinkDTO): CategoryDrinks = with(dto){
        return CategoryDrinks(
            idDrink,
            strDrink,
            strDrinkThumb
        )
    }
}