package com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks

import com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks.CategoryDrinkDTO

data class CategoryDrinksResponse(
    val drinks: List<CategoryDrinkDTO>
)