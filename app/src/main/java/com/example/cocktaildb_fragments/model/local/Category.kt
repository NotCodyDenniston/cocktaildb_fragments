package com.example.cocktaildb_fragments.model.local

data class Category (
    val strCategory: String
)