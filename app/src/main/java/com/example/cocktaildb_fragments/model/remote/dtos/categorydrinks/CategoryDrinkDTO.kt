package com.example.cocktaildb_fragments.model.remote.dtos.categorydrinks

data class CategoryDrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)