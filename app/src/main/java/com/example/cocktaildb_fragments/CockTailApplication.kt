package com.example.cocktaildb_fragments

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CockTailApplication : Application()