package com.example.cocktaildb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb_fragments.model.CocktailRepo
import com.example.cocktaildb_fragments.model.local.Drink
import com.example.cocktaildb_fragments.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class DrinksViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {

    private val _drinks: MutableStateFlow<Resource<List<Drink>>> =
        MutableStateFlow(Resource.Loading())
    val drinkState: StateFlow<Resource<List<Drink>>>
        get() = _drinks



    fun getDrinks(s:String?) = viewModelScope.launch {
        _drinks.value = repo.getDrinks(s = s)
    }
}