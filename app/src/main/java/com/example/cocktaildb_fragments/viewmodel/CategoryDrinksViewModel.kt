package com.example.cocktaildb_fragments.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb_fragments.model.CocktailRepo
import com.example.cocktaildb_fragments.model.local.CategoryDrinks
import com.example.cocktaildb_fragments.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class CategoryDrinksViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {


    private val _categoryDrinks: MutableStateFlow<Resource<List<CategoryDrinks>>> =
        MutableStateFlow(Resource.Loading())
    val categoryDrinkState: StateFlow<Resource<List<CategoryDrinks>>>
        get() = _categoryDrinks


     fun getCategoryDrinks(c:String?) = viewModelScope.launch {
        _categoryDrinks.value = repo.getCategoryDrinks(c = c)
    }
}