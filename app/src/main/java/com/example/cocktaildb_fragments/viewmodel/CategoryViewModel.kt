package com.example.cocktaildb_fragments.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktaildb_fragments.model.CocktailRepo
import com.example.cocktaildb_fragments.model.local.Category
import com.example.cocktaildb_fragments.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {

    private val _categories: MutableStateFlow<Resource<List<Category>>> =
        MutableStateFlow(Resource.Loading())
     val categoryState: StateFlow<Resource<List<Category>>>
        get() = _categories


    fun getCategories() = viewModelScope.launch {
        _categories.value = repo.getCategories()
    }
}