package com.example.cocktaildb_fragments.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.cocktaildb.viewmodel.DrinksViewModel
import com.example.cocktaildb_fragments.R
import com.example.cocktaildb_fragments.model.local.CategoryDrinks
import com.example.cocktaildb_fragments.ui.theme.CockTailDB_FragmentsTheme
import com.example.cocktaildb_fragments.utils.Resource
import com.example.cocktaildb_fragments.viewmodel.CategoryDrinksViewModel


class CategoryDrinksScreen : Fragment(){

    private val categoryDrinksViewModel: CategoryDrinksViewModel by activityViewModels()
    private val drinksViewModel: DrinksViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                CockTailDB_FragmentsTheme {
                    when(val state = categoryDrinksViewModel.categoryDrinkState.collectAsState().value){
                        is Resource.Error -> {}
                        is Resource.Loading -> {}
                        is Resource.Success -> {
                            ShowCategoryDrinksScreenItems(
                                state = state.data,
                                navigate = {findNavController().navigate(R.id.drinks)},
                                viewModel = drinksViewModel)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ShowCategoryDrinksScreenItems(state: List<CategoryDrinks>, navigate: () -> Unit, viewModel: DrinksViewModel){
    Column() {
        Text(text = "WELCOME TO THE CATEGORY OF DRINKS.... MWAHAHAHA👻")
        LazyColumn(){
            items(state){drinkCategory: CategoryDrinks ->
                Button(onClick = {
                    viewModel.getDrinks(drinkCategory.strDrink)
                    navigate() }) {

                    Text(text = drinkCategory.strDrink)
                }
            }
        }

    }
}