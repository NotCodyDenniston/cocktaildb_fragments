package com.example.cocktaildb_fragments.view.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.cocktaildb_fragments.R
import com.example.cocktaildb_fragments.model.local.Category
import com.example.cocktaildb_fragments.ui.theme.CockTailDB_FragmentsTheme
import com.example.cocktaildb_fragments.utils.Resource
import com.example.cocktaildb_fragments.viewmodel.CategoryDrinksViewModel
import com.example.cocktaildb_fragments.viewmodel.CategoryViewModel

class CategoryScreen : Fragment(){

    private val categoryViewModel: CategoryViewModel by activityViewModels()
    private val categoryDrinksViewModel: CategoryDrinksViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                CockTailDB_FragmentsTheme {
                    when(val state = categoryViewModel.categoryState.collectAsState().value){
                        is Resource.Error -> {}
                        is Resource.Loading -> {}
                        is Resource.Success -> {
                            ShowCategoryScreenItems(
                                state = state.data,
                                navigate = {findNavController().navigate(R.id.category_drinks)},
                                viewModel = categoryDrinksViewModel)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun ShowCategoryScreenItems(state: List<Category>, navigate: () -> Unit, viewModel: CategoryDrinksViewModel){
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(text = "Welcome to the Category Screen")
        LazyColumn(verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally) {

            items(state){ category: Category ->
                Button(onClick = {
                    viewModel.getCategoryDrinks(category.strCategory)
                    navigate()
                }){
                    Text(text = category.strCategory)
                }
            }
        }

    }
}